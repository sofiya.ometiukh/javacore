package com.company.lesson2;

public class Main {

    public static void main(String[] args) {
       String channelName = "SUNNY";
       String languageName = "PYTHON";
       System.out.println("Ласкаво просимо на канал " + channelName + ".");
       System.out.println("На каналі " + channelName + " ви знайдете уроки по " + languageName + ".");
       System.out.println(languageName + " є одним із найбільш відомою мовою програмування.");
       
    }
}
